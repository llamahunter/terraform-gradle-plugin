== Introduction

Like many software projects, frameworks, plugins they start off because the author could not find a solution that really fitted their needs and submitting pull requests to existing projects would not have changed the direction of such projects to the needs of the author. This is one of those kind of projects and it is offered against a number of <<alternatives,alternative solutions>> that are available as Gradle plugins.

The aim with the group of plugins is making the integration of https://terraform.io[Terraform] into the build automation pipeline as smooth as possible. This combination of Gradle with Terraform makes for an extremely powerful orchestration platform in the DevOps.

The plugin brings with it a number of subgoals:

* Simplicity to use defaults - convention over configuration
* Maximum flexibility if you need it.
* No need to install `terraform` or relevant tools - let Gradle take care of it for you.

WARNING: This is an incubating project. Until is `1.0` released one day, interfaces and DSL may change between `0.x` releases.

NOTE: These documentation pages assume that you have at least a basic working knowledge of Terraform.

== Terraform + Gradle - A mindset change

Integrating these two products delivers a pwerful combination, but also presents a number of operational ways that challenges people that is used to using `terraform` standalone or in scripts.

Using Terraform with Gradle means that the former no longer needs to be installed. Just specify the version and Gradle will bootstrap the correct version. If the version is already in cache (`~/gradle/native-binaries/`), Gradle will just reuse it.

//=== Reproducibility
//
//In order to reproduce builds a deployments, Gradle will lock down the versions of Terraform plugins in use. This complements the use of version constraints in Terraform configuration files.
//
//Doing the equivalent of `terraform init -update` is not possible directly. See the section on <<terraformlock,Terraform version lock files>> for more details.

=== Caching of plugins

Gradle will cache Terraform plugins using the Terraform caching mechanism, but share it between project by using the Gradle user home caching directories. It is possible to override this a have the caching use a per-project basis or purely rely on global Terrafom settings.



