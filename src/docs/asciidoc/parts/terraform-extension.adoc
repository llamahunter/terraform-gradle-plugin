[[TerraformExtension]]
== Terraform extension

The `terraform` project extension configures a method of obtaining a `terraform` executable. On <<platforms,supported platforms>> it offers the option of configuring a Terraform version, which Gradle will then download, cache and use. THe project extension also allows for the definition of variables at a global level.

Each Terraform task also has a `terraform` extension which can be used to override any settings from the project extension on a per-task basis.

=== Setting Terraform version

It is possible to select another version of Terraform than the default that the plugin will use.

[source,groovy,role="primary"]
----
terraform {
    executable version : '1.2.3' // <1>
    executable path : '/path/to/terraform' // <2>
}
----
<1> Set new Terraform version
<2> Do not bootstrap Terraform, but rather use the binary at the specified location.


