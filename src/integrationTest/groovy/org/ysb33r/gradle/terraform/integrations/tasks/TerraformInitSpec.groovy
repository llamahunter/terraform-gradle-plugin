/*
 * Copyright 2017-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.integrations.tasks

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.gradle.terraform.helpers.DownloadTestSpecification
import org.ysb33r.gradle.terraform.integrations.IntegrationSpecification
import org.ysb33r.grashicorp.HashicorpUtils
import spock.lang.IgnoreIf
import spock.util.environment.RestoreSystemProperties

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

@IgnoreIf({ DownloadTestSpecification.SKIP_TESTS })
@RestoreSystemProperties
class TerraformInitSpec extends IntegrationSpecification {

    String taskName = 'tfInit'
    File testkitDir
    File srcDir
    GradleRunner gradleRunner

    void setup() {
        testkitDir = testProjectDir.newFolder()
        srcDir = new File(projectDir, 'src/tf/main')
        srcDir.mkdirs()

        buildFile.text = '''
        plugins {
            id 'org.ysb33r.terraform'
        }
        '''

        gradleRunner = getGradleRunner(
            IS_GROOVY_DSL,
            projectDir,
            [
                taskName,
                '-s',
            ]
        ).withTestKitDir(testkitDir)
    }

    void 'Run terraform init on a clean project directory'() {
        when:
        BuildResult result = gradleRunner.build()

        then:
        result.task(":${taskName}").outcome == SUCCESS
        new File(testkitDir, 'caches/terraform.d').exists()
    }

    void 'Run terraform init on a project with a single plugin'() {
        setup:
        File pluginDir = new File(testkitDir, "caches/terraform.d/${HashicorpUtils.osArch(OS)}")
        new File(srcDir, 'init.tf').text = '''
provider "aws" {
  version = "~> 2.0"
  region  = "us-east-1"
}
        '''

        when:
        BuildResult result = gradleRunner.build()

        then:
        result.task(":${taskName}").outcome == SUCCESS
        pluginDir.exists()
        pluginDir.listFiles().find { it.name.startsWith('terraform-') }
    }

}